library constants;

const FINISHED_ON_BOARDING = 'finishedOnBoarding';
const COLOR_ACCENT = 0xFF80ffba;
const COLOR_PRIMARY_DARK = 0xFF00b75b;
const COLOR_PRIMARY = 0xFF42EB89;
const FACEBOOK_BUTTON_COLOR = 0xFF415893;
const USERS = 'users';
const PENDING_FRIENDSHIPS = 'pending_friendships';
const CHANNEL_PARTICIPATION = "channel_participation";
const CHANNELS = "channels";
const THREAD = 'thread';
const REPORTS = 'reports';
const SECOND_MILLIS = 1000;
const MINUTE_MILLIS = 60 * SECOND_MILLIS;
const HOUR_MILLIS = 60 * MINUTE_MILLIS;
const SWIPE_COUNT = 'swipe_counts';
const SERVER_KEY =
    "AAAAeliTfEs:APA91bGve5fyExjSiUCB0oI09Br1yGUSb0tPHelAk7L0FUytHWGOMlBPexJubTwSjjJTaIlK7oto3jDevoj9c5Q4Qalk6QEtQ9Y3tYfTxHD7OrmPZuVJjVGGciPBJXThG9QHCZQqx9Id";

const DEFAULT_AVATAR_URL =
    'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg';

const CATEGORIES = 'real_estate_categories';
const LISTINGS = 'real_estate_listings';
const REVIEWS = 'real_estate_reviews';
const FILTERS = 'real_estate_filters';
const SAVED_LISTINGS = 'real_estate_saved_listings';

const GOOGLE_API_KEY = 'AIzaSyAseYt_NDZtWHrdPsz6UZqJZAFbAi_rHic';
