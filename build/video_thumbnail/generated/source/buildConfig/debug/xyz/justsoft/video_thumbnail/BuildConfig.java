/**
 * Automatically generated file. DO NOT MODIFY
 */
package xyz.justsoft.video_thumbnail;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "xyz.justsoft.video_thumbnail";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
